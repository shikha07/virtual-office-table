
view = {
    onStart: function() {
        console.log("onStart...")
        var buttonRun = document.getElementById("run")
        buttonRun.innerText = "Stop"
        buttonRun.removeEventListener("click", this.onStart)
        console.log(this.onStart)
        buttonRun.addEventListener("click", this.onStop)
    },

    onStop: function() {
        console.log("onStop...")
        var buttonRun = document.getElementById("run")
        buttonRun.innerText = "Start"
        buttonRun.removeEventListener("click", this.onStop)
        buttonRun.addEventListener("click", this.onStart)
    },

    init: function() {
        console.log("init...")
        var buttonRun = document.getElementById("run")
        buttonRun.addEventListener("click", this.onStart)
        //this.onStartBoundToThis = this.onStart.bind(this)
        //this.onStopBoundToThis = this.onStop.bind(this)
        //buttonRun.addEventListener("click", this.onStartBoundToThis)
    }
}

window.addEventListener("load", window.view.init.bind(view))
//window.addEventListener("load", window.view.init.bind(window.view))
