
window.model = {

}

window.view = {
    onStop: function() {
         console.log("onStop clicked")
         var buttonRun = document.getElementById("run")
         buttonRun.innerText = "Start"
    },

    onStart: function() {
         console.log("onStart clicked")
         var buttonRun = document.getElementById("run")
         this.innerText = "Stop"
         this.addEventListener("click", view.onStop)
    },

    init: function() {
        var buttonRun = document.getElementById("run")
        console.log(this)
        buttonRun.addEventListener("click", this.onStart)
    }
}


// FIXME: how do I interact with the user???
window.addEventListener("load", window.view.init.bind(window.view))






